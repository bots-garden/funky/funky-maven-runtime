# build
FROM maven:3.6.0-jdk-8-alpine
WORKDIR /home/app

# --- build the application
CMD /bin/bash -c "mvn clean package"
